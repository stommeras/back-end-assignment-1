﻿namespace RPG_Heroes.Enumerators
{
    public enum ArmorType
    {
        Cloth,
        Leather,
        Mail,
        Plate
    }
}
