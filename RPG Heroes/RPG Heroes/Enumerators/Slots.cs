﻿namespace RPG_Heroes.Enumerators
{
    public enum Slot
    {
        Weapon,
        Head,
        Body,
        Legs
    }
}
