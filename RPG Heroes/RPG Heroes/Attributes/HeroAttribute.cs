﻿namespace RPG_Heroes.Attributes
{
    public class HeroAttribute
    {
        public int Strength { get; set; }
        public int Dexterity { get; set; }
        public int Intelligence { get; set; }

        public HeroAttribute(int strength, int dexterity, int intelligence)
        {
            Strength = strength;
            Dexterity = dexterity;
            Intelligence = intelligence;
        }

        /// <summary>
        /// Increases strength by the given amount.
        /// </summary>
        /// <param name="amount"></param>
        public void IncreaseStrength(int amount)
        {
            Strength += amount;
        }

        /// <summary>
        /// Increases dexterity by the given amount.
        /// </summary>
        /// <param name="amount"></param>
        public void IncreaseDexterity(int amount)
        {
            Dexterity += amount;
        }

        /// <summary>
        /// Increases intelligence by the given amount.
        /// </summary>
        /// <param name="amount"></param>
        public void IncreaseIntelligence(int amount)
        {
            Intelligence += amount;
        }
    }
}
