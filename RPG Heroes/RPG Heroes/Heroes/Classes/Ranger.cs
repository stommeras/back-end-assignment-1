﻿using RPG_Heroes.Attributes;
using RPG_Heroes.Enumerators;
using RPG_Heroes.Items.ItemTypes;

namespace RPG_Heroes.Heroes.Classes
{
    public class Ranger : Hero
    {
        public Ranger(string name) : base(name)
        {
            LevelAttributes = new HeroAttribute(1, 7, 1);

            ValidWeaponTypes = new() { WeaponType.Bow };
            ValidArmorTypes = new() { ArmorType.Leather, ArmorType.Mail };
        }

        public override void LevelUp()
        {
            base.LevelUp();
            LevelAttributes.IncreaseStrength(1);
            LevelAttributes.IncreaseDexterity(5);
            LevelAttributes.IncreaseIntelligence(1);
        }

        public override double Damage()
        {
            int weaponDamage = 1;
            if (Equipment[Slot.Weapon] != null)
            {
                Weapon weapon = (Weapon)Equipment[Slot.Weapon];
                weaponDamage = weapon.WeaponDamage;
            }

            HeroAttribute totalAttributes = TotalAttributes;

            int damagingAttribute = totalAttributes.Dexterity; // because ranger

            return weaponDamage * (1 + damagingAttribute / 100);
        }
    }
}
