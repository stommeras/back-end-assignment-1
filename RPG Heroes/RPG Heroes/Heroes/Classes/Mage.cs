﻿using RPG_Heroes.Attributes;
using RPG_Heroes.Enumerators;
using RPG_Heroes.Items.ItemTypes;

namespace RPG_Heroes.Heroes.Classes
{
    public class Mage : Hero
    {
        public Mage(string name) : base(name)
        {
            LevelAttributes = new HeroAttribute(1, 1, 8);

            ValidWeaponTypes = new() { WeaponType.Staff, WeaponType.Wand };
            ValidArmorTypes = new() { ArmorType.Cloth };
        }

        public override void LevelUp()
        {
            base.LevelUp();
            LevelAttributes.IncreaseStrength(1);
            LevelAttributes.IncreaseDexterity(1);
            LevelAttributes.IncreaseIntelligence(5);
        }

        public override double Damage()
        {
            int weaponDamage = 1;
            if (Equipment[Slot.Weapon] != null)
            {
                Weapon weapon = (Weapon)Equipment[Slot.Weapon];
                weaponDamage = weapon.WeaponDamage;
            }

            HeroAttribute totalAttributes = TotalAttributes;

            int damagingAttribute = totalAttributes.Intelligence; // because mage

            return weaponDamage * (1 + damagingAttribute / 100);
        }
    }
}
