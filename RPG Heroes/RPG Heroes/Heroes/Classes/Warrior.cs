﻿using RPG_Heroes.Attributes;
using RPG_Heroes.Enumerators;
using RPG_Heroes.Items.ItemTypes;

namespace RPG_Heroes.Heroes.Classes
{
    public class Warrior : Hero
    {
        public Warrior(string name) : base(name)
        {
            LevelAttributes = new HeroAttribute(5, 2, 1);

            ValidWeaponTypes = new() { WeaponType.Axe, WeaponType.Hammer, WeaponType.Sword };
            ValidArmorTypes = new() { ArmorType.Mail, ArmorType.Plate };
        }

        public override void LevelUp()
        {
            base.LevelUp();
            LevelAttributes.IncreaseStrength(3);
            LevelAttributes.IncreaseDexterity(2);
            LevelAttributes.IncreaseIntelligence(1);
        }

        public override double Damage()
        {
            int weaponDamage = 1;
            if (Equipment[Slot.Weapon] != null)
            {
                Weapon weapon = (Weapon)Equipment[Slot.Weapon];
                weaponDamage = weapon.WeaponDamage;
            }

            HeroAttribute totalAttributes = TotalAttributes;

            int damagingAttribute = totalAttributes.Strength; // because warrior

            return weaponDamage * (1 + damagingAttribute / 100);
        }
    }
}
