﻿using RPG_Heroes.Attributes;
using RPG_Heroes.CustomException;
using RPG_Heroes.Enumerators;
using RPG_Heroes.Items;
using RPG_Heroes.Items.ItemTypes;
using System.Text;

namespace RPG_Heroes.Heroes
{
    public abstract class Hero
    {
        public string Name { get; private set; }
        public int Level { get; private set; }
        public HeroAttribute LevelAttributes { get; set; }
        public HeroAttribute TotalAttributes { get => GetTotalAttributes(); }
        public Dictionary<Slot, Item?> Equipment { get; set; }
        protected List<WeaponType> ValidWeaponTypes { get; set; }
        protected List<ArmorType> ValidArmorTypes { get; set; }

        // It complains about certain things being null, but these are all dealt with in the child classes, as this class is abstract.
        public Hero(string name)
        {
            Name = name;
            Level = 1;
            Equipment = new Dictionary<Slot, Item?>()
            {
                { Slot.Head, null },
                { Slot.Body, null },
                { Slot.Legs, null },
                { Slot.Weapon, null }
            };
        }

        /// <summary>
        /// Increases <c>Level</c> by one, and increases <c>HeroAttribute</c> levels based on the class.
        /// </summary>
        public virtual void LevelUp()
        {
            Level++;
        }

        /// <summary>
        /// Attempts to wield the weapon provided as argument. Throws custom exception if not in <c>ValidWeaponTypes</c> or not high enough level.
        /// </summary>
        /// <param name="weapon"></param>
        /// <exception cref="InvalidWeaponException"></exception>
        public void Equip(Weapon weapon)
        {
            if (!ValidWeaponTypes.Contains(weapon.WeaponType))
            {
                throw new InvalidWeaponException("Your class cannot wield this type of weapon.");
            }
            if (Level < weapon.RequiredLevel)
            {
                throw new InvalidWeaponException($"You must be level {weapon.RequiredLevel} to wield this weapon.");
            }

            Equipment[weapon.Slot] = weapon;
        }

        /// <summary>
        /// Attempts to equip the armor provided as argument. Throws custom exception if not in <c>ValidArmorTypes</c> or not high enough level.
        /// </summary>
        /// <param name="armor"></param>
        /// <exception cref="InvalidArmorException"></exception>
        public void Equip(Armor armor)
        {
            if (!ValidArmorTypes.Contains(armor.ArmorType))
            {
                throw new InvalidArmorException("Your class cannot equip this type of armor.");
            }
            if (Level < armor.RequiredLevel)
            {
                throw new InvalidArmorException($"You must be level {armor.RequiredLevel} to equip this armor.");
            }

            Equipment[armor.Slot] = armor;
        }

        /// <summary>
        /// Gets total <c>HeroAttribute</c> levels including equipped armor. Gets called in the getter of the prop <c>TotalAttributes</c>
        /// </summary>
        /// <returns></returns>
        public HeroAttribute GetTotalAttributes()
        {
            HeroAttribute attributes = LevelAttributes;

            foreach (KeyValuePair<Slot, Item?> item in Equipment)
            {
                if (item.Value is Armor armor && item.Value != null)
                {
                    attributes.IncreaseStrength(armor.ArmorAttribute.Strength);
                    attributes.IncreaseDexterity(armor.ArmorAttribute.Dexterity);
                    attributes.IncreaseIntelligence(armor.ArmorAttribute.Intelligence);
                }
            }

            return attributes;
        }

        /// <summary>
        /// Calculates current damage done based on class and equipped weapon.
        /// </summary>
        /// <returns></returns>
        public abstract double Damage();

        /// <summary>
        /// Outputs information about the Hero.
        /// </summary>
        /// <returns></returns>
        public string Display()
        {
            HeroAttribute totalAttributes = TotalAttributes;

            StringBuilder builder = new();
            builder.Append("Name: ").Append(Name).AppendLine();
            builder.Append("Class: ").Append(GetType().Name).AppendLine();
            builder.Append("Level: ").Append(Level).AppendLine();
            builder.Append("Strength: ").Append(totalAttributes.Strength).AppendLine();
            builder.Append("Dexterity: ").Append(totalAttributes.Dexterity).AppendLine();
            builder.Append("Intelligence: ").Append(totalAttributes.Intelligence).AppendLine();
            builder.Append("Damage: ").Append(Damage()).AppendLine();

            return builder.ToString();
        }
    }
}
