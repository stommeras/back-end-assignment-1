﻿namespace RPG_Heroes.CustomException
{
    public class InvalidWeaponException : Exception
    {
        public InvalidWeaponException(string? message) : base(message)
        {
        }
    }
}
