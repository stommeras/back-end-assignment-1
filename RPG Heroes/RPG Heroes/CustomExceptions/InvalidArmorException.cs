﻿namespace RPG_Heroes.CustomException
{
    public class InvalidArmorException : Exception
    {
        public InvalidArmorException(string? message) : base(message)
        {
        }
    }
}
