using RPG_Heroes.Attributes;
using RPG_Heroes.Heroes.Classes;

namespace RPG_Heroes_Test
{
    public class HeroLevelTests
    {
        [Fact]
        public void Warrior_OnCreation_HasCorrectLevel()
        {
            Warrior warrior = new("Steffen");
            int expected = 1;
            int actual = warrior.Level;
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Ranger_OnCreation_HasCorrectLevel()
        {
            Ranger ranger = new("Steffen");
            int expected = 1;
            int actual = ranger.Level;
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Mage_OnCreation_HasCorrectLevel()
        {
            Mage mage = new("Steffen");
            int expected = 1;
            int actual = mage.Level;
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Rogue_OnCreation_HasCorrectLevel()
        {
            Rogue rogue = new("Steffen");
            int expected = 1;
            int actual = rogue.Level;
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Warrior_OnLevelUp_GainsOneLevel()
        {
            Warrior warrior = new("Steffen");
            warrior.LevelUp();

            int expected = 2;
            int actual = warrior.Level;

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Mage_OnLevelUp_GainsOneLevel()
        {
            Mage mage = new("Steffen");
            mage.LevelUp();

            int expected = 2;
            int actual = mage.Level;

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Ranger_OnLevelUp_GainsOneLevel()
        {
            Ranger ranger = new("Steffen");
            ranger.LevelUp();

            int expected = 2;
            int actual = ranger.Level;

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Rogue_OnLevelUp_GainsOneLevel()
        {
            Rogue rogue = new("Steffen");
            rogue.LevelUp();

            int expected = 2;
            int actual = rogue.Level;

            Assert.Equal(expected, actual);
        }
    }

    public class HeroAttributeTests { 

        [Fact]
        public void HeroAttributes_Warrior_OnCreation_InitialValuesCorrect()
        {
            Warrior warrior = new("Steffen");

            HeroAttribute expected = new(5, 2, 1);
            HeroAttribute actual = warrior.TotalAttributes;

            Assert.Equivalent(expected, actual);
        }

        [Fact]
        public void HeroAttributes_Rogue_OnCreation_InitialValuesCorrect()
        {
            Rogue rogue = new("Steffen");

            HeroAttribute expected = new(2, 6, 1);
            HeroAttribute actual = rogue.TotalAttributes;

            Assert.Equivalent(expected, actual);
        }

        [Fact]
        public void HeroAttributes_Ranger_OnCreation_InitialValuesCorrect()
        {
            Ranger ranger = new("Steffen");

            HeroAttribute expected = new(1, 7, 1);
            HeroAttribute actual = ranger.TotalAttributes;

            Assert.Equivalent(expected, actual);
        }

        [Fact]
        public void HeroAttributes_Mage_OnCreation_InitialValuesCorrect()
        {
            Mage mage = new("Steffen");

            HeroAttribute expected = new(1, 1, 8);
            HeroAttribute actual = mage.TotalAttributes;

            Assert.Equivalent(expected, actual);
        }

        [Fact]
        public void LevelUp_Warrior_IncreasesLevelAttributesCorrectly()
        {
            Warrior warrior = new("Steffen");
            warrior.LevelUp();

            HeroAttribute expected = new(8, 4, 2);
            HeroAttribute actual = warrior.LevelAttributes;

            Assert.Equivalent(expected, actual);

        }

        [Fact]
        public void LevelUp_Mage_IncreasesLevelAttributesCorrectly()
        {
            Mage mage = new("Steffen");
            mage.LevelUp();

            HeroAttribute expected = new(2, 2, 13);
            HeroAttribute actual = mage.LevelAttributes;

            Assert.Equivalent(expected, actual);

        }

        [Fact]
        public void LevelUp_Rogue_IncreasesLevelAttributesCorrectly()
        {
            Rogue rogue = new("Steffen");
            rogue.LevelUp();

            HeroAttribute expected = new(3, 10, 2);
            HeroAttribute actual = rogue.LevelAttributes;

            Assert.Equivalent(expected, actual);

        }

        [Fact]
        public void LevelUp_Ranger_IncreasesLevelAttributesCorrectly()
        {
            Ranger ranger = new("Steffen");
            ranger.LevelUp();

            HeroAttribute expected = new(2, 12, 2);
            HeroAttribute actual = ranger.LevelAttributes;

            Assert.Equivalent(expected, actual);

        }

    }
}