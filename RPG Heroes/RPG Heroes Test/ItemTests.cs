﻿using RPG_Heroes.Enumerators;
using RPG_Heroes.Heroes.Classes;
using RPG_Heroes.Items.ItemTypes;
using RPG_Heroes.Attributes;
using RPG_Heroes.CustomException;

namespace RPG_Heroes_Test
{
    public class CreatingWeapon
    {
        // feels like i should be able to check all attributes of the object without several asserts, but can't find it
        [Fact]
        public void CreatedWeaponGetsCorrectName()
        {
            Weapon dragonDagger = new("Dragon dagger", 3, Slot.Weapon, WeaponType.Dagger, 10);
            string expected = "Dragon dagger";
            string actual = dragonDagger.Name;
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void CreatedWeaponGetsCorrectRequiredLevel()
        {
            Weapon dragonDagger = new("Dragon dagger", 3, Slot.Weapon, WeaponType.Dagger, 10);
            int expected = 3;
            int actual = dragonDagger.RequiredLevel;
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void CreatedWeaponGetsCorrectSlot()
        {
            Weapon dragonDagger = new("Dragon dagger", 3, Slot.Weapon, WeaponType.Dagger, 10);
            Slot expected = Slot.Weapon;
            Slot actual = dragonDagger.Slot;
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void CreatedWeaponGetsCorrectWeaponType()
        {
            Weapon dragonDagger = new("Dragon dagger", 3, Slot.Weapon, WeaponType.Dagger, 10);
            WeaponType expected = WeaponType.Dagger;
            WeaponType actual = dragonDagger.WeaponType;
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void CreatedWeaponGetsCorrectWeaponDamage()
        {
            Weapon dragonDagger = new("Dragon dagger", 3, Slot.Weapon, WeaponType.Dagger, 10);
            int expected = 10;
            int actual = dragonDagger.WeaponDamage;
            Assert.Equal(expected, actual);
        }
    }

    public class CreatingArmor
    {
        [Fact]
        public void CreatedArmorGetsCorrectName()
        {
            Armor bandosPlatebody = new("Bandos platebody", 3, Slot.Body, ArmorType.Plate, new HeroAttribute(5, 0, 0));
            string expected = "Bandos platebody";
            string actual = bandosPlatebody.Name;
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void CreatedArmorGetsCorrectRequiredLevel()
        {
            Armor bandosPlatebody = new("Bandos platebody", 3, Slot.Body, ArmorType.Plate, new HeroAttribute(5, 0, 0));
            int expected = 3;
            int actual = bandosPlatebody.RequiredLevel;
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void CreatedArmorGetsCorrectSlot()
        {
            Armor bandosPlatebody = new("Bandos platebody", 3, Slot.Body, ArmorType.Plate, new HeroAttribute(5, 0, 0));
            Slot expected = Slot.Body;
            Slot actual = bandosPlatebody.Slot;
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void CreatedArmorGetsCorrectArmorType()
        {
            Armor bandosPlatebody = new("Bandos platebody", 3, Slot.Body, ArmorType.Plate, new HeroAttribute(5, 0, 0));
            ArmorType expected = ArmorType.Plate;
            ArmorType actual = bandosPlatebody.ArmorType;
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void CreatedArmorGetsCorrectArmorAttributes()
        {
            Armor bandosPlatebody = new("Bandos platebody", 3, Slot.Body, ArmorType.Plate, new HeroAttribute(5, 0, 0));
            HeroAttribute expected = new(5, 0, 0);
            HeroAttribute actual = bandosPlatebody.ArmorAttribute;
            Assert.Equivalent(expected, actual);
        }
    }

    public class EquippingWeapon
    {
        [Fact]
        public void EquippingWeaponWithWrongType_ShouldThrowInvalidWeaponException()
        {
            Warrior worldGuardian = new("Cow1337killr");
            Weapon bronzeDagger = new("Bronze Dagger", 1, Slot.Weapon, WeaponType.Dagger, 1);
            string expected = "Your class cannot wield this type of weapon.";

            Exception exception = Assert.Throws<InvalidWeaponException>(() => { worldGuardian.Equip(bronzeDagger); });
            string actual = exception.Message;
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void EquippingWeaponWithTooHighLevelRequirement_ShouldThrowInvalidWeaponException()
        {
            Warrior worldGuardian = new("Cow1337killr");
            Weapon dragonWarhammer = new("Dragon Warhammer", 2, Slot.Weapon, WeaponType.Hammer, 1);
            string expected = "You must be level 2 to wield this weapon.";

            Exception exception = Assert.Throws<InvalidWeaponException>(() => { worldGuardian.Equip(dragonWarhammer); });
            string actual = exception.Message;
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void EquippingWeaponShouldEquipWeapon()
        {
            Warrior worldGuardian = new("Cow1337killr");
            Weapon bronzeLongsword = new("Bronze Longsword", 1, Slot.Weapon, WeaponType.Sword, 4);
            worldGuardian.Equip(bronzeLongsword);

            Weapon expected = new("Bronze Longsword", 1, Slot.Weapon, WeaponType.Sword, 4);
            Weapon actual = (Weapon)worldGuardian.Equipment[Slot.Weapon];
            Assert.Equivalent(expected, actual);
        }
    }

    public class EquippingArmor
    {
        [Fact]
        public void EquippingArmorWithWrongType_ShouldThrowInvalidArmorException()
        {
            Warrior worldGuardian = new("Cow1337killr");
            Armor fancySweater = new("Fancy Sweater", 1, Slot.Body, ArmorType.Cloth, new HeroAttribute(0, 0, 0));

            string expected = "Your class cannot equip this type of armor.";

            Exception exception = Assert.Throws<InvalidArmorException>(() => { worldGuardian.Equip(fancySweater); });
            string actual = exception.Message;
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void EquippingArmorWithTooHighLevelRequirement_ShouldThrowInvalidArmorException()
        {
            Warrior worldGuardian = new("Cow1337killr");
            Armor dragonPlatebody = new("Dragon Platebody", 2, Slot.Body, ArmorType.Plate, new HeroAttribute(3, 0, 0));
            string expected = "You must be level 2 to equip this armor.";

            Exception exception = Assert.Throws<InvalidArmorException>(() => { worldGuardian.Equip(dragonPlatebody); });
            string actual = exception.Message;
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void EquippingArmorShouldEquipArmor()
        {
            Warrior worldGuardian = new("Cow1337killr");
            Armor bronzePlatebody = new("Bronze Platebody", 1, Slot.Body, ArmorType.Plate, new HeroAttribute(1, 0, 0));
            worldGuardian.Equip(bronzePlatebody);

            Armor expected = new("Bronze Platebody", 1, Slot.Body, ArmorType.Plate, new HeroAttribute(1, 0, 0));
            Armor actual = (Armor)worldGuardian.Equipment[Slot.Body];

            Assert.Equivalent(expected, actual);
        }
    }

    public class TotalAttributes
    {
        [Fact]
        public void TotalAttributes_WithNoArmorEquipped()
        {
            Warrior worldGuardian = new("Cow1337killr");

            HeroAttribute expected = new(5, 2, 1);
            HeroAttribute actual = worldGuardian.TotalAttributes;

            Assert.Equivalent(expected, actual);
        }

        [Fact]
        public void TotalAttributes_WithOnePieceOfArmorEquipped()
        {
            Warrior worldGuardian = new("Cow1337killr");
            Armor bronzePlatebody = new("Bronze Platebody", 1, Slot.Body, ArmorType.Plate, new HeroAttribute(1, 0, 0));
            worldGuardian.Equip(bronzePlatebody);

            HeroAttribute expected = new(6, 2, 1);
            HeroAttribute actual = worldGuardian.TotalAttributes;

            Assert.Equivalent(expected, actual);
        }

        [Fact]
        public void TotalAttributes_WithTwoPiecesOfArmorEquipped()
        {
            Warrior worldGuardian = new("Cow1337killr");
            Armor bronzePlatebody = new("Bronze Platebody", 1, Slot.Body, ArmorType.Plate, new HeroAttribute(1, 0, 0));
            Armor bronzePlatelegs = new("Bronze Platelegs", 1, Slot.Legs, ArmorType.Plate, new HeroAttribute(1, 0, 0));
            worldGuardian.Equip(bronzePlatebody);
            worldGuardian.Equip(bronzePlatelegs);

            HeroAttribute expected = new(7, 2, 1);
            HeroAttribute actual = worldGuardian.TotalAttributes;

            Assert.Equivalent(expected, actual);
        }

        [Fact]
        public void TotalAttributes_WithOneReplacedPieceOfArmorEquipped()
        {
            Warrior worldGuardian = new("Cow1337killr");
            Armor bronzePlatebody = new("Bronze Platebody", 1, Slot.Body, ArmorType.Plate, new HeroAttribute(1, 0, 0));
            Armor bronzeChainbody = new("Bronze Chainbody", 1, Slot.Body, ArmorType.Mail, new HeroAttribute(0, 1, 0));

            worldGuardian.Equip(bronzePlatebody);
            worldGuardian.Equip(bronzeChainbody);

            HeroAttribute expected = new(5, 3, 1);
            HeroAttribute actual = worldGuardian.TotalAttributes;

            Assert.Equivalent(expected, actual);
        }
    }

    public class HeroDamage
    {
        [Fact]
        public void CalculateHeroDamage_WithNoWeaponEquipped()
        {
            Warrior worldGuardian = new("Cow1337killr");
            double expected = 1 * (1 + (5 / 100));
            double actual = worldGuardian.Damage();

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void CalculateHeroDamage_WithWeaponEquipped()
        {
            Warrior worldGuardian = new("Cow1337killr");
            Weapon bronzeLongsword = new("Bronze Longsword", 1, Slot.Weapon, WeaponType.Sword, 4);
            worldGuardian.Equip(bronzeLongsword);

            double expected = 4 * (1 + (5 / 100));
            double actual = worldGuardian.Damage();

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void CalculateHeroDamage_WithReplacedWeaponEquipped()
        {
            Warrior worldGuardian = new("Cow1337killr");
            Weapon bronzeLongsword = new("Bronze Longsword", 1, Slot.Weapon, WeaponType.Sword, 4);
            Weapon bronzeBattleaxe = new("Bronze Battleaxe", 1, Slot.Weapon, WeaponType.Axe, 6);
            worldGuardian.Equip(bronzeLongsword);
            worldGuardian.Equip(bronzeBattleaxe);

            double expected = 6 * (1 + (5 / 100));
            double actual = worldGuardian.Damage();

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void CalculateHeroDamage_WithWeaponAndArmorEquipped()
        {
            Warrior worldGuardian = new("Cow1337killr");
            Weapon bronzeLongsword = new("Bronze Longsword", 1, Slot.Weapon, WeaponType.Sword, 4);
            Armor bronzePlatebody = new("Bronze Platebody", 1, Slot.Body, ArmorType.Plate, new HeroAttribute(1, 0, 0));

            worldGuardian.Equip(bronzeLongsword);
            worldGuardian.Equip(bronzePlatebody);

            double expected = 4 * (1 + ((5 + 1) / 100));
            double actual = worldGuardian.Damage();

            Assert.Equal(expected, actual);
        }
    }
}
